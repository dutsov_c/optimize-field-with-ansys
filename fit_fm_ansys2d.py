import sys
import os

import numpy as np
import matplotlib.pyplot as plt
from dataclasses import dataclass
from lmfit.models import Model
from lmfit import Parameters, minimize, report_fit, fit_report, Minimizer

from pyaedt import Maxwell2d

SETUP = "MagSetup2D"

proj = Maxwell2d("helix")
proj.set_active_design("MagneticDesign2D")
proj.cleanup_solution()
proj.clean_proj_folder()
varmanager = proj.variable_manager

mag_width = varmanager.variables["$magnet_width"].numeric_value / 2.0

def objective(params, x, data, data_err):
    num_dims = len(data)
    resid = [data[i] * 0.0 for i in range(num_dims)]
    gen_new_field_map(params)
    sim_fmap = simulated_field_map(params, x)

    for i in range(num_dims):
        resid[i] = (data[i] - sim_fmap[i])/data_err[i]

    resid_coinc = np.concatenate([[resid.x, resid.y, resid.z] for resid in resid])
    print("Quadratic average of residuals (std. devs): ", np.sqrt(sum(resid_coinc**2) / num_dims))
    return resid_coinc


def gen_new_field_map(params):
    print("================== setting new parameters ===============")
    for key, value in params.items():
        varname = '$' + key
        cur_expr = varmanager.get_expression(varname)
        if isinstance(cur_expr, str):
            varunit = ''.join([i for i in cur_expr if not (i.isdigit() or i == '.' or i == '-') ])
        else:
            varunit = ""
        expr = str(value.value) + varunit
        print(varname, ": ", expr)
        varmanager.set_variable(varname, expression=expr, overwrite=True)
    print("======= done =======")
    proj.analyze_setup(SETUP)


def simulated_field_map(params, coords):
    sample_points_lists = list()
    x_off = params['x_offset'].value
    y_off = params['y_offset'].value
    # z_off = params['z_offset'].value
    off = vec(x_off, y_off, 0.0)
    for c in coords:
        c = c + off
        sample_points_lists.append([c.x / 1000.0, 0.0, (c.y + mag_width) / 1000.0])

    fname = os.path.join(os.getcwd(), "tmp.dat")
    #fname = "C:/Users/dutsov_c/Documents/test"
    proj.post.export_field_file("B_vector",
           filename=fname,
           sample_points_lists=sample_points_lists,
           solution="MagSetup2D : LastAdaptive",
           export_with_sample_points=True
    )
    simulated_field_map = list()
    with open(fname) as f:
        f.readline()
        for line in f:
            s = line.split()
            f = vec(float(s[3]), float(s[5]), 0.0)
            simulated_field_map.append(f)
    simulated_field_map = np.asarray(simulated_field_map)
    # proj.cleanup_solution()
    # proj.clean_proj_folder()
    return simulated_field_map


def main():
    fname = ''
    if len(sys.argv) == 2:
        fname = sys.argv[1]
    else:
        fname = './field_map_data_fin.dat'

    field_map = read_map(fname)

    params = Parameters()

    params.add('main_coil_windings_density', value=31.5, min=30.5, max=32.5, vary=True)
    params.add('main_coil_inner_r', value=110.9, min=110, max=140, vary=True)
    params.add('main_coil_width', value=880, min=800, max=975, vary=True)
    # params.add('main_coil_thickness', value=5, min=10, max=40, vary=True)

    params.add('trim_coil_windings_density', value=6.7, min=5.5, max=8, vary=True)
    params.add('trim_coil_radius', value=107, min=105.0, max=110, vary=True)
    params.add('trim_coil_width', value=69.52, min=60, max=80, vary=True)
    params.add('trim_coil_pos', value=345.0, min=320, max=360, vary=True)
    # params.add('trim_coil_thickness', value=2, min=1, max=40, vary=True)

    params.add('trim_coil2_windings_density', value=0.498, min=0.4, max=0.6, vary=True)
    # params.add('trim_coil2_radius', value=110, min=105.0, max=125, vary=True)
    params.add('trim_coil2_width', value=95.64, min=90, max=100, vary=True)
    # params.add('trim_coil2_pos', value=248, min=150, max=350, vary=False)
    # params.add('trim_coil2_thickness', value=2, min=1, max=40, vary=False)

    params.add('trim_coil3_windings', value=-2, min=-200.0, max=200, vary=True)
    # params.add('trim_coil2_radius', value=110, min=105.0, max=125, vary=True)
    params.add('trim_coil3_width', value=50, min=0, max=100, vary=True)
    # params.add('trim_coil2_pos', value=248, min=150, max=350, vary=False)
    # params.add('trim_coil2_thickness', value=2, min=1, max=40, vary=False)



    params.add('x_offset', value=0, min=-20, max=20, vary=False)
    params.add('y_offset', value=-15.9, min=-20, max=20, vary=True)
    # params.add('z_offset', value=-0.82, min=-20, max=20)

    coords = [x for (x, y) in field_map]
    field = [y for (x, y) in field_map]
    field_err = [1.0/1000.0 if np.abs(x.y) < 200 else 0.01 for (x, y) in field_map]

    minim = Minimizer(objective, params, fcn_args=(coords, field, field_err))
    out = minim.scalar_minimize(
                   method='CG',
                   options={'eps':0.005})
    print(fit_report(out))


def read_map(fname):
    field_map = []
    with open(fname) as f:
        for line in f:
            s = line.split()
            if s[0] == 'X,mm':
                continue
            rho = float(s[0])
            phi = float(s[1])
            if not phi == 0:
                continue
            
            x = float(s[2])
            y = float(s[4])
            z = float(s[3])

            bx = float(s[5])
            by = float(s[7])
            bz = float(s[6])

            lim = 1320
            if -lim < y < lim:
                coord = vec(x, y, z)
                field = vec(bx, by, bz)
                field_map.append((coord, field))

    return field_map

@dataclass
class vec():
    x: float
    y: float
    z: float

    def __add__(self, other):
        if isinstance(other, vec):
            x = self.x + other.x
            y = self.y + other.y
            z = self.z + other.z
            return vec(x, y, z)

    def __sub__(self, other):
        if isinstance(other, vec):
            x = self.x - other.x
            y = self.y - other.y
            z = self.z - other.z
            return vec(x, y, z)

    def __mul__(self, other):
        if isinstance(other, float) or isinstance(other, int):
            x = self.x * other
            y = self.y * other
            z = self.z * other
            return vec(x, y, z)

    def __truediv__(self, other):
        if isinstance(other, float):
            x = self.x / other
            y = self.y / other
            z = self.z / other
            return vec(x, y, z)
        if isinstance(other, vec):
            x = self.x / other.x
            y = self.y / other.y
            z = self.z / other.z
            return vec(x, y, z)

    def __matmul__(self, other):
        if isinstance(other, float):
            x = self.x * other
            y = self.y * other
            z = self.z * other
            return vec(x, y, z)

if __name__ == "__main__":
    main()
