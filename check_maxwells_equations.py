import sys
import os

import numpy as np
import matplotlib.pyplot as plt
from dataclasses import dataclass

def read_map(fname):
    coords = []
    fields = []
    with open(fname) as f:
        f.readline()
        f.readline()
        for line in f:
            s = line.split()
            x = float(s[0])
            y = float(s[2])
            z = float(s[1])

            bx = float(s[3])
            by = float(s[5])
            bz = float(s[4])

            if (y == 0.0):
                coord = [x, z, y]
                field = [bx, bz, by]
                coords.append(coord)
                fields.append(field)

    return coords, fields


def divergence(f, sp):
    """ Computes divergence of vector field
    f: array -> vector field components [Fx,Fy,Fz,...]
    sp: array -> spacing between points in respecitve directions [spx, spy,spz,...]
    """
    # print(np.gradient(f[0], 0.005, axis=0) + np.gradient(f[1], 0.005, axis=2) + np.gradient(f[2], 0.005, axis=2))
    return np.ufunc.reduce(np.add, [np.gradient(f[i], sp[i])[i] for i in range(len(f))])

def main():
    fname = '../../../Projects/muon-edm-simulation/Geant4/helix/fieldmaps/WF_coil_med.map'
    coords, fields = read_map(fname)
    cols = int(200/2+1)
    colsy = int(400/4+1)

    fx = np.array([f[0] for f in fields])
    fy = np.array([f[1] for f in fields])
    # fz = np.array([f[2] for f in fields])

    x = np.array([c[0] for c in coords])
    y = np.array([c[1] for c in coords])
    z = np.array([c[2] for c in coords])

    # print(x)
    # print(y)
    # print(z)

    xx = 0.002
    yy = 0.004
    x = x.reshape((cols, colsy))
    y = y.reshape((cols, colsy))
    fx = fx.reshape((cols, colsy))
    fy = fy.reshape((cols, colsy))
    # fz = fz.reshape((cols, cols))

    f = [fx, fy]
    sp = [xx, yy]

    div = divergence(f, sp)
    print(div)

    print(x[:,0])

    plt.pcolormesh(x, y, div)
    plt.show()
    plt.hist(div.flatten(), bins=100, log=True)
    plt.show()


@dataclass
class vec():
    x: float
    y: float
    z: float

    def __add__(self, other):
        if isinstance(other, vec):
            x = self.x + other.x
            y = self.y + other.y
            z = self.z + other.z
            return vec(x, y, z)

    def __sub__(self, other):
        if isinstance(other, vec):
            x = self.x - other.x
            y = self.y - other.y
            z = self.z - other.z
            return vec(x, y, z)

    def __mul__(self, other):
        if isinstance(other, float) or isinstance(other, int):
            x = self.x * other
            y = self.y * other
            z = self.z * other
            return vec(x, y, z)

    def __truediv__(self, other):
        if isinstance(other, float):
            x = self.x / other
            y = self.y / other
            z = self.z / other
            return vec(x, y, z)
        if isinstance(other, vec):
            x = self.x / other.x
            y = self.y / other.y
            z = self.z / other.z
            return vec(x, y, z)

    def __matmul__(self, other):
        if isinstance(other, float):
            x = self.x * other
            y = self.y * other
            z = self.z * other
            return vec(x, y, z)

if __name__ == "__main__":
    main()
