from pyaedt import Maxwell3d

proj = Maxwell3d()

varmanager = proj.variable_manager
mag_width = varmanager.variables["$magnet_width"].numeric_value / 2.0

fname = 'C:/Users/dutsov_c/Documents/WF_coil_test.map'
proj.set_active_design("MagneticDesign")
#fname = 'C:/Users/dutsov_c/Documents/EF_coil.map'
#proj.set_active_design("ElectroDesign")

x_lim = 400
y_lim = 1000
z_lim = 400
grid_step = [2, 2, 2]

proj.post.export_field_file("B_nonsmooth",
                            filename=fname,
                            solution="MagSetup: LastAdaptive")


#proj.post.export_field_file_on_grid("B_vector",
#        filename=fname,
#        solution="MagSetup_fine: LastAdaptive",
#        grid_start=[-x_lim,-y_lim + mag_width,-z_lim],
#        grid_stop=[x_lim, y_lim + mag_width, z_lim],
#        grid_step=grid_step,
#        isvector=True
#        )
#proj.post.export_field_file_on_grid("E_vector",
#        filename=fname,
#        solution="ElSetup: LastAdaptive",
#        grid_start=[-x_lim,-y_lim + mag_width,-z_lim],
#        grid_stop=[x_lim, y_lim + mag_width, z_lim],
#        grid_step=grid_step,
#        isvector=True
#        )
