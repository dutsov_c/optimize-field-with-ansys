reset session

set terminal qt font "Helvetica, 25"

fname = './field_map_data_fin.dat'
sim_name = './tmp.dat'

set title "B-field Y component (longitudinal)"
set xlabel "Distance from center, mm"
set ylabel "Magnetic B-field strength, T"
set key bottom left

set xrange [-500:]

xoff = 1.22
yoff = 17

RAD=0
plot\
     fname u ($5 - yoff):($1 == 55 ? $8 : NaN):($8*0.000346) w yerr t sprintf('R = %d mm', 55) ls 5 pt 7,\
     sim_name u ($3 * 1000 - 500):($1 > 5.2e-2 && $1 < 5.9e-2 ? $6 : NaN) ls 5 w lp smooth unique t 'sim 55 mm',\
     fname u ($5 - yoff):($1 == 68 ? $8 : NaN):($8*0.000346) w yerr t sprintf('R = %d mm', 68) ls 7 pt 7,\
     sim_name u ($3 * 1000 - 500):($1 > 67e-3 && $1 < 71.5e-3 ? $6 : NaN) ls 7 w lp smooth unique t 'sim 68 mm'
     # fname u ($5 - yoff):($1 == 0 && $2 == 0 ? $8 : NaN):($8*0.000346) w yerr t sprintf('R = %d mm', 0) ls 1 pt 7,\
     # sim_name u ($3 * 1000 - 500):( ($1 < 1e-2 && $1 > -1e-2) ? $6 : NaN) w lp smooth unique ls 1 t 'sim 0 mm',\
     # fname u ($5 - yoff):($1 == 30 && $2 == 90 ? $8 : NaN):($8*0.000346) w yerr t sprintf('R = %d mm', 33) ls 2 pt 7,\
     # sim_name u ($3 * 1000 - 500):($1 > 29e-3 && $1 < 32e-3 ? $6 : NaN) w lp smooth unique ls 2 t 'sim 33 mm',\

     # fname u ($5 - yoff):($1 == 0 ? $8 : NaN):($8*0.000346) w yerr t sprintf('R = %d mm', RAD) ls 7 pt 7,\
     # fname u ($5 - yoff):($1 == 67 || $1 == 68 ? $8 : NaN):($8*0.000346) w yerr t sprintf('R > %d mm', 68) ls 2 pt 7,\
     # fname u ($5 - yoff):($1 == 30 || $1 == 30 ? $8 : NaN):($8*0.000346) w yerr t sprintf('R = %d mm', 30) ls 3 pt 7,\
